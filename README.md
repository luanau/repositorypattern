# README #

Example Repository Pattern from http://www.programmingwithmosh.com/

### What is this repository for? ###

* Demonstrate the Repository Pattern

### How do I get set up? ###

Add Sql Server Express  
Add/populate the database with,
```
Add-Migration Initial
Update-Database
```